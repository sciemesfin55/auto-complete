/* eslint-disable */

import Home from "../components/Home.vue";
import Table from '../components/dataTable/DataTable';

export const routes = [
  {
    path: "/",
    component: Home
  },
  {
    path: "/dataTable",
    component: Table
  },
  {
    path: "*",
    redirect: "/"
  }
];
