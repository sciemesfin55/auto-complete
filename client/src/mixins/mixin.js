import axios from "axios";

const url = "http://localhost:3000/api/regions";
const countUrl = "http://localhost:3000/api/regions/count";

export const mixin = {
  data: () => ({
    totalregions: 0,
    regions: [],
    loading: true,
    pagination: {},
    regions: [],
    noPages: "",
    headers: [
      {
        text: "Region Name",
        align: "left",
        value: "name"
      },
      { text: "phone", value: "Phone" },
      { text: "Actions", value: "name", sortable: false }
    ]
  }),
  watch: {
    pagination: {
      handler() {
        this.getDataFromApi().then(data => {
          this.regions = data.items;
          this.totalregions = data.total;
        });
      },
      deep: true
    }
  },
  mounted() {
    this.getDataFromApi().then(data => {
      this.regions = data.items;
      this.totalregions = data.total;
    });
  },
  methods: {
    getDataFromApi() {
      this.loading = true;
      return new Promise((resolve, reject) => {
        const { page, rowsPerPage } = this.pagination;
        axios
          .get(url)
          .then(res => {
            let items = res.data;

            const total = items.length;

            if (rowsPerPage > 0) {
              items = items.slice((page - 1) * rowsPerPage, page * rowsPerPage);
            }
            setTimeout(() => {
              this.loading = false;
              resolve({
                items,
                total
              });
            }, 700);
          })
          .catch(error => {
            console.log(error);
          });
      });
    },

    deleteRegion(regionId) {
      axios.delete(url + "/" + regionId)
        .then(() => {
          alert("Region Deleted");
          this.$router.go();
        })
        .catch(console.log("Unable to delete data."));
    }
  }
};
